require('dotenv').config()
const morgan = require('morgan')
const express = require('express')
const cors = require('cors')
const app = express();

const PORT = 3000;

const CORS_OPTIONS = { origin: true, optionsSuccessStatus: 200 }
app.use(cors(CORS_OPTIONS));
app.use(express.urlencoded({extended: true}))
app.use(express.json({extended: true}));
app.use(morgan('dev'));

app.get('/', (req, res) => {
    res.send('Software Avanzado - Original - Práctica 4 - GrupoK')
})

app.listen(PORT, () => {
    console.log(`Servicio corriendo en ${PORT}`)
})
